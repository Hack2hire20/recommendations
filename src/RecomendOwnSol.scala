object RecomendOwnSol
{
/usr/bin/spark-shell



/ SQLContext entry point for working with structured data
val sqlContext = new org.apache.spark.sql.SQLContext(sc)
import sqlContext.implicits._
import org.apache.spark.sql._
import org.apache.spark.mllib.recommendation.{ALS,
  MatrixFactorizationModel, Rating}

  
// input format Ratings
case class Ratings(user_id: String, venue_id: String, rating: String)
case class Socialgraph(first_user_id: String, second_user_id: String)
case class Users(id: String, latitude: String,longitude: String)  
case class Venues(id: String, latitude: String,longitude: String)  
case class Visits(id:String, user_id:String, venue_id:String, latitude:String, longitude:String, created_at:String)

  
def parseRatings(str: String): Ratings = {
             val fields = str.split('|')
      Ratings(fields(0).toString.trim, fields(1).toString.trim, fields(2).toString.trim)
 }	
def parseSocialgraph(str: String): Socialgraph = {
             val fields = str.split('|')
      Socialgraph(fields(0).toString.trim, fields(1).toString.trim)
 }	
def parseUsers(str: String): Users = {
             val fields = str.split('|')
      Users(fields(0).toString.trim, fields(1).toString.trim, fields(2).toString.trim)
 }	
def parseVenues(str: String): Venues = {
             val fields = str.split('|')
      Venues(fields(0).toString.trim, fields(1).toString.trim, fields(2).toString.trim)
 }	
def parseVisits(str: String): Visits = {
             val fields = str.split('|')
      Visits(fields(0).toString.trim, fields(1).toString.trim, fields(2).toString.trim, fields(3).toString.trim, fields(4).toString.trim, fields(5).toString.trim)
 }	



 
val ratingText = sc.textFile("s3://challenge2extract/ratings.psv")	  
val SocialgraphText = sc.textFile("s3://challenge2extract/socialgraph.psv")	  
val UsersText = sc.textFile("s3://challenge2extract/users.psv")	  
val VenuesText = sc.textFile("s3://challenge2extract/venues.psv")	  
val VisitsText = sc.textFile("s3://challenge2extract/visits.psv")	  

val ratingsRDD = ratingText.map(parseRatings).cache()	 
val socialgraphRDD = ratingText.map(parseSocialgraph).cache()	 
val usersRDD = ratingText.map(parseUsers).cache()	 
val venuesRDD = ratingText.map(parseVenues).cache()	 
val visitsRDD = ratingText.map(parseVisits).cache()	 

val ratingsDF = ratingsRDD.toDF()
val socialgraphDF = socialgraphRDD.toDF()
val usersDF = usersRDD.toDF()
val venuesDF = venuesRDD.toDF()
val visitsDF = visitsRDD.toDF()


ratingsDF.registerTempTable("ratings") 
socialgraphDF.registerTempTable("socialgraph") 
usersDF.registerTempTable("users") 
venuesDF.registerTempTable("venues") 
visitsDF.registerTempTable("visits") 

ratingsDF.printSchema()
  
/ Get the max, min ratings along with the count of users who have
// rated a movie.
val results = sqlContext.sql(
  """select venues.id,venues.latitude,venues.longitude,Ratings.rating from venues
left join ratings 
on ratings.venue_id=venues.id
left join users
on users.id=ratings.user_id
where users.id=2
order by 
venues.latitude-users.latitude,venues.longitude-users.longitude,Ratings.rating desc""")

// DataFrame show() displays the top 20 rows in  tabular form
results.show()
}
  
  	