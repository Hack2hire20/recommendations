package com.hack2hire.services;

import java.util.ArrayList;
import java.util.List;

import com.hack2hire.GPSHelper;
import com.hack2hire.Hack2HireContants;
import com.hack2hire.Rating;
import com.hack2hire.User;
import com.hack2hire.Venue;
import com.hack2hire.dao.UserDao;

public class UserService {

	private UserDao userDao;

	public UserService() {
		userDao = new UserDao();
	}

	public User getUser(Integer userId) {

		return userDao.getUser();

	}
	
	
	public void addReview(User user,Rating rating){
		
		userDao.addReview( user, rating);
	}

	public List<Venue> getNearVenues(User user) {

		double userLat = user.getHomeLocation().getLattitude();
		double userLong = user.getHomeLocation().getLongitude();

		List<Venue> venueList = userDao.getNearVenues(("" + userLat).split(".")[0], ("" + userLong).split(".")[0]);

		List<Venue> nearVenueList = new ArrayList<Venue>();

		for (Venue venue : venueList) {

			double dist = GPSHelper.DistanceBetInKM(userLat, userLong, venue.getLocation().getLattitude(),
					venue.getLocation().getLongitude());

			if (dist <= Hack2HireContants.NEAR_DISTANCE) {

				nearVenueList.add(venue);
			}

		}

		return nearVenueList;

	}

	public List<Venue> getRecommendedVenues(User user) {

		List<Venue> nearVenueList = getNearVenues(user);
		
		
		
		
		return null;

	}

}
