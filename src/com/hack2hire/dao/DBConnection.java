package com.hack2hire.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	private String connectionURL = "jdbc:oracle:thin:@rec20team.cwmgfhyr8fbd.us-west-2.rds.amazonaws.com:1521:ORCL";
	private String username = "rec_20_team";
	private String password = "rec20team";

	public Connection getConnection() {

		Connection connection = null;

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			connection = DriverManager.getConnection(connectionURL, username, password);

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return connection;

	}

}
