package com.hack2hire.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.hack2hire.Rating;
import com.hack2hire.User;
import com.hack2hire.Venue;

public class UserDao extends DBConnection {

	public User getUser() {

		String sql = "select * from users";

		try {

			Statement stmt = getConnection().prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	public List<Venue> getNearVenues(String latitude, String longitude) {

		String sql = "select * from venues where latitude like ? and longitude like ?";

 		try {

			PreparedStatement stmt = getConnection().prepareStatement(sql);
			stmt.setString(1, latitude + ".%");
			stmt.setString(2, longitude + ".%");

			ResultSet rs = stmt.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	public void addReview(User user, Rating rating) {
		
	}

}
