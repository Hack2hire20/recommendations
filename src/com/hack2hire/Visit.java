package com.hack2hire;
import java.sql.Timestamp;

public class Visit {

	private Integer visitId;
	private Timestamp vistedTime;
	private Integer venueId;

	public Integer getVisitId() {
		return visitId;
	}

	public void setVisitId(Integer visitId) {
		this.visitId = visitId;
	}

	public Timestamp getVistedTime() {
		return vistedTime;
	}

	public void setVistedTime(Timestamp vistedTime) {
		this.vistedTime = vistedTime;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

}
