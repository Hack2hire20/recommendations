package com.hack2hire;

import java.util.List;

/**
 * 
 */

/**
 * @author home
 *
 */

public class User {

	private String userId;
	private List<Integer> socialGraphList;
	private Location homeLocation;
	private List<Rating> ratings;
	private List<Visit> checkins;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<Integer> getSocialGraphList() {
		return socialGraphList;
	}

	public void setSocialGraphList(List<Integer> socialGraphList) {
		this.socialGraphList = socialGraphList;
	}

	public Location getHomeLocation() {
		return homeLocation;
	}

	public void setHomeLocation(Location homeLocation) {
		this.homeLocation = homeLocation;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public List<Visit> getCheckins() {
		return checkins;
	}

	public void setCheckins(List<Visit> checkins) {
		this.checkins = checkins;
	}

}
