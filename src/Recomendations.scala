object Recomendations {

 val sqlContext = new org.apache.spark.sql.SQLContext(sc)
 import sqlContext.implicits._
 import org.apache.spark.sql._
 import org.apache.spark.mllib.recommendation.{ALS,
 MatrixFactorizationModel, Rating}
 
val ratingText = sc.textFile("s3://challenge2extract/ratings.psv")

 val ratingsRDD = ratingText.map(_.split('|').map(_.trim) match {
    case Array(user, venue, rate) =>  Rating(user.toInt, venue.toInt, rate.toInt)
})

val splits = ratingsRDD.randomSplit(Array(0.8, 0.2), 0L)

val trainingRatingsRDD = splits(0).cache()
val testRatingsRDD = splits(1).cache()

val numTraining = trainingRatingsRDD.count()
val numTest = testRatingsRDD.count()
println(s"Training: $numTraining, test: $numTest.")

// build a ALS user venue matrix model with rank=20, iterations=10
val model = ALS.train(ratingsRDD, 1, 20, 0.01)
  
// Evaluate the model on rating data
val usersVenues = ratingsRDD.map{ case Rating(user, venue, rate)  => (user, venue)}
val predictions = model.predict(usersVenues).map{
    case Rating(user, venue, rate) => ((user, venue), rate)
}
val tetsAndPreds = ratingsRDD.map{
    case Rating(user, venue, rate) => ((user, venue), rate)
}.join(predictions)

val MSE = tetsAndPreds.map{
    case ((user, product), (r1, r2)) =>  math.pow((r1- r2), 2)
}.reduce(_ + _)/tetsAndPreds.count

println("Mean Squared Error = " + MSE)  

}
  